FROM node:14

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY ./dist .dist

EXPOSE 80
run npm run start:prod