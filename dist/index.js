"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_1 = require("discord.js");
const database_service_1 = require("./services/database/database.service");
const env = require("./env/environment.json");
const messages_listener_service_1 = require("./services/messages/messages-listener.service");
const mentions_listener_service_1 = require("./services/mentions/mentions-listener.service");
const logger_service_1 = require("./services/logger/logger.service");
const reactions_listener_service_1 = require("./services/reactions/reactions-listener.service");
const client = new discord_js_1.Client();
const _logger = new logger_service_1.LoggerService();
let _databaseService = new database_service_1.default(_logger);
let _messagesListener = new messages_listener_service_1.MessagesListenerService(client, _logger, _databaseService);
let _mentionsListener = new mentions_listener_service_1.MentionsListenerService(client, _logger);
let _reactionsListener = new reactions_listener_service_1.ReactionsListenerService(client, _logger);
client.on('ready', function () {
    _logger.info('[Downtown Cab.] Downtown Cab. discord bot ready.');
});
_messagesListener.listenServicesMessages();
_messagesListener.listenReportsMessages();
_messagesListener.listenDeleteMessages();
_messagesListener.listenAccountingMessages();
_messagesListener.listenManagmentMessages();
_mentionsListener.listenBotMentions();
_reactionsListener.listenValidationsMessages();
client.login(env.token);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwyQ0FBb0M7QUFDcEMsMkVBQW1FO0FBQ25FLDhDQUE4QztBQUM5Qyw2RkFBd0Y7QUFDeEYsNkZBQXdGO0FBQ3hGLHFFQUFpRTtBQUNqRSxnR0FBMkY7QUFFM0YsTUFBTSxNQUFNLEdBQVcsSUFBSSxtQkFBTSxFQUFFLENBQUM7QUFDcEMsTUFBTSxPQUFPLEdBQUcsSUFBSSw4QkFBYSxFQUFFLENBQUM7QUFFcEMsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLDBCQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7QUFFcEQsSUFBSSxpQkFBaUIsR0FBRyxJQUFJLG1EQUF1QixDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztBQUN2RixJQUFJLGlCQUFpQixHQUFHLElBQUksbURBQXVCLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0FBQ3JFLElBQUksa0JBQWtCLEdBQUcsSUFBSSxxREFBd0IsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7QUFFdkUsTUFBTSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUU7SUFDZixPQUFPLENBQUMsSUFBSSxDQUFDLGtEQUFrRCxDQUFDLENBQUM7QUFDckUsQ0FBQyxDQUFDLENBQUM7QUFFSCxpQkFBaUIsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0FBQzNDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLENBQUM7QUFDMUMsaUJBQWlCLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztBQUN6QyxpQkFBaUIsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO0FBQzdDLGlCQUFpQixDQUFDLHVCQUF1QixFQUFFLENBQUM7QUFFNUMsaUJBQWlCLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztBQUV0QyxrQkFBa0IsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO0FBRS9DLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDIn0=