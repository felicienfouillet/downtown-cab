"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_1 = require("firebase");
const firebase_config_1 = require("../../env/firebase/firebase.config");
class DataBaseService {
    constructor(_logger) {
        this._logger = _logger;
        this.init();
        this._logger.info('[Downtown Cab.] Database service initialized.');
    }
    ;
    init() {
        firebase_1.default.initializeApp(firebase_config_1.config);
        this.database = firebase_1.default.database();
        this._logger.info('[Firebase] App initialized.');
    }
    async setOne(ref, data) {
        return firebase_1.default.database().ref(ref).set(data);
    }
    getOne(ref) {
        return firebase_1.default.database().ref(ref).once('value').then(snapshot => {
            return snapshot.val();
        });
    }
    getAll(childRef) {
        return firebase_1.default.database().ref(childRef).once('value').then(snapshot => {
            return snapshot.val();
        });
    }
    deleteOne(ref, callbackMessage) {
        return this.setOne(ref, null).then(() => {
            return callbackMessage;
        });
    }
    deleteAll(childRef, callbackMessage) {
        return this.setOne(childRef, null).then(() => {
            return callbackMessage;
        });
    }
    generateUUID() {
        return '_' + Math.random().toString(36).substr(2, 9);
    }
}
exports.default = DataBaseService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YWJhc2Uuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9zZXJ2aWNlcy9kYXRhYmFzZS9kYXRhYmFzZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsdUNBQWdDO0FBQ2hDLHdFQUE0RDtBQUc1RCxNQUFxQixlQUFlO0lBR2hDLFlBQW9CLE9BQXNCO1FBQXRCLFlBQU8sR0FBUCxPQUFPLENBQWU7UUFDdEMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRVosSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsK0NBQStDLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBQUEsQ0FBQztJQUVNLElBQUk7UUFDUixrQkFBUSxDQUFDLGFBQWEsQ0FBQyx3QkFBTSxDQUFDLENBQUM7UUFFL0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxrQkFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRXBDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVNLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLElBQUk7UUFDekIsT0FBTyxrQkFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVNLE1BQU0sQ0FBQyxHQUFHO1FBQ2IsT0FBTyxrQkFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzlELE9BQU8sUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLE1BQU0sQ0FBQyxRQUFRO1FBQ2xCLE9BQU8sa0JBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNuRSxPQUFPLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSxTQUFTLENBQUMsR0FBRyxFQUFFLGVBQWU7UUFDakMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ3BDLE9BQU8sZUFBZSxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLFNBQVMsQ0FBQyxRQUFRLEVBQUUsZUFBZTtRQUN0QyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDekMsT0FBTyxlQUFlLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sWUFBWTtRQUNmLE9BQU8sR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN6RCxDQUFDO0NBQ0o7QUFoREQsa0NBZ0RDIn0=