"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerService = void 0;
const env = require("../../env/environment.json");
class LoggerService {
    constructor() { }
    log(message) {
        if (!env.prod) {
            console.log(message);
        }
    }
    info(message) {
        console.info(message);
    }
    error(message) {
        console.error(message);
    }
}
exports.LoggerService = LoggerService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nZ2VyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvc2VydmljZXMvbG9nZ2VyL2xvZ2dlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLGtEQUFrRDtBQUVsRCxNQUFhLGFBQWE7SUFDdEIsZ0JBQWdCLENBQUM7SUFFVixHQUFHLENBQUMsT0FBWTtRQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRTtZQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDeEI7SUFDTCxDQUFDO0lBRU0sSUFBSSxDQUFDLE9BQVk7UUFDcEIsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRU0sS0FBSyxDQUFDLE9BQVk7UUFDckIsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMzQixDQUFDO0NBQ0o7QUFoQkQsc0NBZ0JDIn0=