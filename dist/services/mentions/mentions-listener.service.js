"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MentionsListenerService = void 0;
const discord_js_1 = require("discord.js");
const env = require("../../env/environment.json");
class MentionsListenerService {
    constructor(client, _logger) {
        this.client = client;
        this._logger = _logger;
        this._logger.info('[Downtown Cab.] Mentions listener loaded.');
    }
    listenBotMentions() {
        this.client.on('message', message => {
            const channel = message.channel;
            if (channel.id == env.channels.help.id) {
                if (message.mentions.has(env.botId)) {
                    const embed = new discord_js_1.MessageEmbed();
                    embed.setTitle("Help center !")
                        .setAuthor("Liste des commandes", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                        .setColor(0)
                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setTimestamp();
                    embed.fields = [
                        {
                            'name': `󠀠`,
                            'value': `󠀠`,
                            'inline': false
                        },
                        {
                            'name': `Afficher la liste des commandes`,
                            'value': `Mentionner simplement le client **Downtown Cab.**\n󠀠`,
                            'inline': false
                        },
                        {
                            'name': `Signaler une prise de service`,
                            'value': `***!start*** ou ***!s***\n󠀠`,
                            'inline': false
                        },
                        {
                            'name': `Signaler une fin de service`,
                            'value': `***!end*** ou ***!e***\n󠀠`,
                            'inline': false
                        },
                        {
                            'name': `Faire un rapport de service`,
                            'value': `***!report*** [Temps de service -> ***...h...m*** | ***..H...M***]\n [Nombre de courses citoyennes -> ***...***]\n [Montant des courses citoyennes -> ***...***]\n [Montant des courses PNJ (sans les pourboires) -> ***...***]\n\n ou ***!r*** [...]`,
                            'inline': false
                        },
                        {
                            'name': `󠀠`,
                            'value': `󠀠`,
                            'inline': false
                        }
                    ];
                    let driver = this.client.users.cache.find(user => user.id === message.author.id);
                    let roles = message.mentions.members.first().roles.cache;
                    let rolesList = [];
                    roles.map(r => {
                        if (r.name != '@everyone') {
                            rolesList.push(`${r.id}`);
                        }
                    });
                    if (rolesList.filter(r => (driver.id == env.roles.director || r.id == env.roles.coDirector || r.id == env.roles.manager)).length > 0) {
                        embed.fields[embed.fields.length - 1] = {
                            'name': `󠀠\n\nCalculer une prime`,
                            'value': `󠀠!p @ [pseudo_discord_chauffeur] [pourcentage_de_prime]\nou !prime @ [pseudo_discord_chauffeur] [pourcentage_de_prime]`,
                            'inline': false
                        };
                        embed.fields.push({
                            'name': `󠀠Informations chauffeur`,
                            'value': `󠀠!i @ [pseudo_discord_chauffeur]\nou !info @ [pseudo_discord_chauffeur]`,
                            'inline': false
                        });
                        embed.fields.push({
                            'name': `󠀠`,
                            'value': `󠀠`,
                            'inline': false
                        });
                    }
                    channel.send(embed);
                }
            }
        });
    }
}
exports.MentionsListenerService = MentionsListenerService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudGlvbnMtbGlzdGVuZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9zZXJ2aWNlcy9tZW50aW9ucy9tZW50aW9ucy1saXN0ZW5lci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDJDQUFrRDtBQUNsRCxrREFBa0Q7QUFHbEQsTUFBYSx1QkFBdUI7SUFDaEMsWUFBb0IsTUFBYyxFQUFVLE9BQXNCO1FBQTlDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUFlO1FBQzlELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLDJDQUEyQyxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUVNLGlCQUFpQjtRQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLEVBQUU7WUFDaEMsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztZQUVoQyxJQUFJLE9BQU8sQ0FBQyxFQUFFLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUNwQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDakMsTUFBTSxLQUFLLEdBQWlCLElBQUkseUJBQVksRUFBRSxDQUFDO29CQUUvQyxLQUFLLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQzt5QkFDMUIsU0FBUyxDQUFDLHFCQUFxQixFQUFFLDhEQUE4RCxFQUFFLG9CQUFvQixDQUFDO3lCQUN0SCxRQUFRLENBQUMsQ0FBQyxDQUFDO3lCQUNYLFNBQVMsQ0FBQyxlQUFlLEVBQUUsK0VBQStFLENBQUM7eUJBQzNHLFlBQVksQ0FBQywrRUFBK0UsQ0FBQzt5QkFDN0YsWUFBWSxFQUFFLENBQUM7b0JBRXBCLEtBQUssQ0FBQyxNQUFNLEdBQUc7d0JBQ1g7NEJBQ0ksTUFBTSxFQUFFLElBQUk7NEJBQ1osT0FBTyxFQUFFLElBQUk7NEJBQ2IsUUFBUSxFQUFFLEtBQUs7eUJBQ2xCO3dCQUNEOzRCQUNJLE1BQU0sRUFBRSxpQ0FBaUM7NEJBQ3pDLE9BQU8sRUFBRSx1REFBdUQ7NEJBQ2hFLFFBQVEsRUFBRSxLQUFLO3lCQUNsQjt3QkFNRDs0QkFDSSxNQUFNLEVBQUUsK0JBQStCOzRCQUN2QyxPQUFPLEVBQUUsOEJBQThCOzRCQUN2QyxRQUFRLEVBQUUsS0FBSzt5QkFDbEI7d0JBQ0Q7NEJBQ0ksTUFBTSxFQUFFLDZCQUE2Qjs0QkFDckMsT0FBTyxFQUFFLDRCQUE0Qjs0QkFDckMsUUFBUSxFQUFFLEtBQUs7eUJBQ2xCO3dCQUNEOzRCQUNJLE1BQU0sRUFBRSw2QkFBNkI7NEJBQ3JDLE9BQU8sRUFBRSx1UEFBdVA7NEJBQ2hRLFFBQVEsRUFBRSxLQUFLO3lCQUNsQjt3QkFDRDs0QkFDSSxNQUFNLEVBQUUsSUFBSTs0QkFDWixPQUFPLEVBQUUsSUFBSTs0QkFDYixRQUFRLEVBQUUsS0FBSzt5QkFDbEI7cUJBQ0osQ0FBQztvQkFFRixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUVqRixJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO29CQUN6RCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7b0JBRW5CLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ1YsSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLFdBQVcsRUFBRTs0QkFDdkIsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO3lCQUM3QjtvQkFDTCxDQUFDLENBQUMsQ0FBQztvQkFFSCxJQUFJLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLElBQUksR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO3dCQUNqSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxHQUFHOzRCQUNwQyxNQUFNLEVBQUUsMEJBQTBCOzRCQUNsQyxPQUFPLEVBQUUseUhBQXlIOzRCQUNsSSxRQUFRLEVBQUUsS0FBSzt5QkFDbEIsQ0FBQzt3QkFFRixLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQzs0QkFDZCxNQUFNLEVBQUUsMEJBQTBCOzRCQUNsQyxPQUFPLEVBQUUsMEVBQTBFOzRCQUNuRixRQUFRLEVBQUUsS0FBSzt5QkFDbEIsQ0FBQyxDQUFDO3dCQUVILEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDOzRCQUNkLE1BQU0sRUFBRSxJQUFJOzRCQUNaLE9BQU8sRUFBRSxJQUFJOzRCQUNiLFFBQVEsRUFBRSxLQUFLO3lCQUNsQixDQUFDLENBQUM7cUJBQ047b0JBRUQsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDdkI7YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztDQUNKO0FBOUZELDBEQThGQyJ9