"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessagesListenerService = void 0;
const discord_js_1 = require("discord.js");
const env = require("../../env/environment.json");
class MessagesListenerService {
    constructor(client, _logger, _databaseService) {
        this.client = client;
        this._logger = _logger;
        this._databaseService = _databaseService;
        this._logger.info('[Downtown Cab.] Messages listener loaded.');
    }
    listenServicesMessages() {
        this.client.on('message', message => {
            const channel = message.channel;
            const authorId = message.author.id;
            if (channel.id == env.channels.services.id) {
                if (message.content === '!s' || message.content === '!start') {
                    const embed = new discord_js_1.MessageEmbed();
                    embed.setAuthor("Prise de service", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                        .setDescription(`<@!${authorId}>`)
                        .setColor(65280)
                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setTimestamp();
                    channel.send(embed);
                }
                if (message.content === '!e' || message.content === '!end') {
                    const embed = new discord_js_1.MessageEmbed();
                    embed.setAuthor("Fin de service", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                        .setDescription(`<@!${authorId}>`)
                        .setColor(16711680)
                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setTimestamp();
                    channel.send(embed);
                }
            }
        });
    }
    listenReportsMessages() {
        this.client.on('message', message => {
            const channel = message.channel;
            const authorId = message.author.id;
            if (channel.id == env.channels.reports.id) {
                const args = message.content.split(' ');
                const amountFormat = Intl.NumberFormat('fr-FR');
                if (args[0] === '!r' || args[0] === '!report') {
                    if (args.length === 5) {
                        if (args[1].match(/^(?=\d+[hmHM])((\d+[hH])?(?!\d)?(\d+[mM])?(?!\d))?$/g)) {
                            if (args[2].match(/^(?=\d)(\d*)?$/g)) {
                                if (args[3].match(/^[0-9]{0,9}(\.[0-9]{1,2})?$/g) && args[4].match(/^[0-9]{0,9}(\.[0-9]{1,2})?$/g)) {
                                    const cytizenAmount = amountFormat.format(parseFloat(args[3]));
                                    const pnjAmount = amountFormat.format(parseFloat(args[4]));
                                    const uuid = this._databaseService.generateUUID();
                                    const embed = new discord_js_1.MessageEmbed().setAuthor("Rapport de service", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                        .setDescription(`<@!${authorId}>`)
                                        .setColor(131586)
                                        .setFooter(`Downtown Cab. • ${uuid}`, "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();
                                    embed.fields = [
                                        {
                                            'name': `Temps de travail`,
                                            'value': `${args[1]}`,
                                            'inline': true
                                        },
                                        {
                                            'name': `Nombre de courses`,
                                            'value': `${args[2]}\n󠀠`,
                                            'inline': true
                                        },
                                        {
                                            'name': `Courses citoyennes`,
                                            'value': `${cytizenAmount} $`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses PNJ            󠀠`,
                                            'value': `${pnjAmount} $`,
                                            'inline': true
                                        },
                                        {
                                            'name': `Total`,
                                            'value': `${amountFormat.format(parseFloat(args[3]) + parseFloat(args[4]))} $`,
                                            'inline': true
                                        },
                                        {
                                            'name': `󠀠`,
                                            'value': `󠀠`,
                                            'inline': false
                                        }
                                    ];
                                    const ref = 'reports/' + uuid + '/';
                                    const data = {
                                        employeeId: message.author.id,
                                        workingTime: args[1],
                                        runCount: args[2],
                                        cytizenAmount: cytizenAmount,
                                        pnjAmount: pnjAmount,
                                        timestamp: Date.now(),
                                        status: 0
                                    };
                                    this._databaseService.setOne(ref, data);
                                    channel.send(embed);
                                }
                                else {
                                    const content = 'Invalid amount count';
                                    channel.send(content);
                                }
                            }
                            else {
                                const content = 'Invalid run count';
                                channel.send(content);
                            }
                        }
                        else {
                            const content = 'Invalid time format';
                            channel.send(content);
                        }
                    }
                    else {
                        if (args.length < 5) {
                            const content = 'Missing arguments';
                            channel.send(content);
                        }
                        else {
                            const content = 'Invalid arguments';
                            channel.send(content);
                        }
                    }
                }
            }
        });
    }
    listenDeleteMessages() {
        this.client.on('message', message => {
            const channel = message.channel;
            if (channel.id == env.channels.reports.id) {
                const args = message.content.split(' ');
                if (args[0] === '!d' || args[0] === '!delete') {
                    if (message.member.roles.cache.find(r => r.id === env.roles.director)) {
                        if (args.length === 1) {
                            this._databaseService.deleteAll('reports/', `Tous les rapports ont été supprimés`).then(() => {
                                this._logger.info('[Downtown Cab.] Delete all reports.');
                                const embed = new discord_js_1.MessageEmbed();
                                embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                    .setDescription(`Tous les rapports ont été supprimés`)
                                    .setColor(65280)
                                    .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                    .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                    .setTimestamp();
                                channel.send(embed);
                            });
                        }
                        if (args.length === 2) {
                            this._databaseService.getOne(`reports/${args[1]}/`).then(value => {
                                if (value) {
                                    this._databaseService.deleteOne('reports/' + args[1] + '/', `Le rapports **${args[1]}** a été supprimé`).then(callbackMessage => {
                                        this._logger.info('[Downtown Cab.] Delete report:');
                                        this._logger.info({ id: args[1], value: value });
                                        const embed = new discord_js_1.MessageEmbed();
                                        embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                            .setDescription(callbackMessage)
                                            .setColor(65280)
                                            .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                            .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                            .setTimestamp();
                                        channel.send(embed);
                                    });
                                }
                                else {
                                    const embed = new discord_js_1.MessageEmbed();
                                    this._logger.error(`[Downtown Cab.] Report ${args[1]} was not found`);
                                    embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                        .setDescription(`Le rapport **${args[1]}** est introuvable`)
                                        .setColor(16711680)
                                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();
                                    channel.send(embed);
                                }
                            });
                        }
                    }
                    else {
                        const embed = new discord_js_1.MessageEmbed();
                        embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                            .setDescription(`Vous n'avez pas la permission pour utiliser cette commande !`)
                            .setColor(16711680)
                            .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                            .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                            .setTimestamp();
                        channel.send(embed);
                    }
                }
            }
        });
    }
    listenAccountingMessages() {
        this.client.on('message', message => {
            const channel = message.channel;
            const employee = message.mentions.users.first();
            if (channel.id == env.channels.accounting.id) {
                const args = message.content.split(' ');
                if (args[0] === '!p' || args[0] === '!prime') {
                    if (args.length === 3) {
                        this._databaseService.getAll('reports/').then(data => {
                            if (data) {
                                let compiledData = [];
                                for (let i = 0; i < Object.keys(data).length; i++) {
                                    compiledData.push({
                                        key: Object.keys(data)[i],
                                        data: Object.values(data)[i]
                                    });
                                }
                                const result = compiledData.filter(elements => (elements.data['employeeId'] == employee.id && elements.data['status'] == 0));
                                if (result.length > 0) {
                                    const timestamps = result.map(v => v.data['timestamp']);
                                    const workingTime = result.map(v => v.data['workingTime']);
                                    const totalWorkingTime = this.compileWorkingTime(workingTime);
                                    const runCount = result.map(v => parseInt(v.data['runCount']));
                                    const totalRunCount = this.sum(runCount);
                                    const pnjAmount = this.compileAmount(result.map(v => v.data['pnjAmount']));
                                    const totalPnjAmount = this.sum(pnjAmount);
                                    const formatedTotalPnjAmount = this.formatAmount(totalPnjAmount);
                                    const cytizenAmount = this.compileAmount(result.map(v => v.data['cytizenAmount']));
                                    const totalCytizenAmount = this.sum(cytizenAmount);
                                    const formatedTotalCytizenAmount = this.formatAmount(totalCytizenAmount);
                                    const totalAmount = totalPnjAmount + totalCytizenAmount;
                                    const formatedTotalAmount = this.formatAmount(totalAmount);
                                    const embed = new discord_js_1.MessageEmbed();
                                    embed.setAuthor("Prévisualisation de prime", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                        .setDescription(`Prime de <@!${employee.id}>`)
                                        .setColor(8421504)
                                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();
                                    embed.fields = [
                                        {
                                            'name': `Periode`,
                                            'value': `Du ${this.formatTimestamp(new Date(Math.min(...timestamps)))} au ${this.formatTimestamp(new Date(Math.max(...timestamps)))}\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Temps de travail`,
                                            'value': `${totalWorkingTime.time.formated}`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Nombre de courses`,
                                            'value': `${totalRunCount}\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses PNJ`,
                                            'value': `${formatedTotalPnjAmount}$`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses citoyennes`,
                                            'value': `${formatedTotalCytizenAmount}$\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Total`,
                                            'value': `${formatedTotalAmount}$\n󠀠`,
                                            'inline': true
                                        },
                                        {
                                            'name': `Primes (${args[2]}%)`,
                                            'value': `${this.formatAmount((totalAmount * parseInt(args[2])) / 100)}$\n\n󠀠`,
                                            'inline': true
                                        }
                                    ];
                                    channel.send(embed).then(msg => {
                                        try {
                                            msg.react('✔️');
                                            msg.react('❌');
                                            msg.awaitReactions((reaction, user) => !user.bot && (reaction.emoji.name == '✔️' || reaction.emoji.name == '❌'), { max: 1, time: 15000 }).then(collected => {
                                                let reaction = collected.first();
                                                if (reaction.users.reaction.emoji.name == '✔️') {
                                                    const uuid = this._databaseService.generateUUID();
                                                    this._logger.info('[Downtown Cab.] Reaction validation validated:');
                                                    this._logger.info({
                                                        reason: 'Validated',
                                                        bonusId: uuid,
                                                        messageId: msg.id
                                                    });
                                                    let embed = reaction.message.embeds[0];
                                                    embed.setAuthor('Prime enregistrée', 'https://yagami.xyz/content/uploads/2018/11/discord-512-1.png', 'https://yagami.xyz')
                                                        .setDescription(`Prime de <@!${message.mentions.users.first().id}>`)
                                                        .setColor(16754176)
                                                        .setFooter(`Downtown Cab. • ${uuid}`, "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png");
                                                    result.forEach(report => {
                                                        this._databaseService.setOne(`reports/${report.key}/status`, 1);
                                                    });
                                                    const ref = 'bonuses/' + uuid + '/';
                                                    const data = {
                                                        employeeId: message.mentions.users.first().id,
                                                        workingTime: totalWorkingTime,
                                                        runCount: totalRunCount,
                                                        cytizenAmount: totalCytizenAmount,
                                                        pnjAmount: totalPnjAmount,
                                                        totalAmount: totalAmount,
                                                        percentage: parseInt(args[2]),
                                                        prime: (totalAmount * parseInt(args[2])) / 100,
                                                        timestamp: Date.now(),
                                                    };
                                                    this._databaseService.setOne(ref, data);
                                                    msg.edit(embed);
                                                }
                                                else {
                                                    this._logger.info('[Downtown Cab.] Reaction validation canceled:');
                                                    this._logger.info({
                                                        reason: 'Canceled',
                                                        messageId: msg.id
                                                    });
                                                    let embed = reaction.message.embeds[0];
                                                    embed.setAuthor('Calcule de prime annulé', 'https://yagami.xyz/content/uploads/2018/11/discord-512-1.png', 'https://yagami.xyz')
                                                        .setColor(16711680);
                                                    msg.edit(embed);
                                                }
                                                msg.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
                                            }).catch((reason) => {
                                                this._logger.error('[Downtown Cab.] Reaction validation canceled: timed out');
                                                this._logger.error({
                                                    reason: 'Timed out',
                                                    messageId: msg.id
                                                });
                                                let embed = msg.embeds[0];
                                                embed.setAuthor('Calcule de prime annulé (catch)', 'https://yagami.xyz/content/uploads/2018/11/discord-512-1.png', 'https://yagami.xyz')
                                                    .setColor(16711680);
                                                msg.edit(embed);
                                                msg.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
                                            });
                                        }
                                        catch (error) {
                                            console.error('Fail to react:', error);
                                        }
                                    });
                                }
                                else {
                                    const embed = new discord_js_1.MessageEmbed();
                                    this._logger.error(`[Downtown Cab.] Reports not founds`);
                                    this._logger.error({
                                        reason: 'Not Found',
                                        messageId: message.id
                                    });
                                    embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                        .setDescription(`Tous les rapports de services de <@!${employee.id}> ont déjà été traité`)
                                        .setColor(16711680)
                                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();
                                    channel.send(embed);
                                }
                            }
                        });
                    }
                    else {
                        if (args.length < 3) {
                            const content = 'Missing arguments';
                            channel.send(content);
                        }
                        else {
                            const content = 'Invalid arguments';
                            channel.send(content);
                        }
                    }
                }
            }
        });
    }
    listenManagmentMessages() {
        this.client.on('message', message => {
            const channel = message.channel;
            const employee = message.mentions.users.first();
            if (channel.id == env.channels.employeeManagment.id) {
                const args = message.content.split(' ');
                if (args[0] === '!i' || args[0] === '!info') {
                    if (args.length === 2) {
                        this._databaseService.getAll('reports/').then(data => {
                            if (data) {
                                let compiledData = [];
                                for (let i = 0; i < Object.keys(data).length; i++) {
                                    compiledData.push({
                                        key: Object.keys(data)[i],
                                        data: Object.values(data)[i]
                                    });
                                }
                                const result = compiledData.filter(elements => (elements.data['employeeId'] == employee.id));
                                if (result.length > 0) {
                                    const timestamps = result.map(v => v.data['timestamp']);
                                    const workingTime = result.map(v => v.data['workingTime']);
                                    const totalWorkingTime = this.compileWorkingTime(workingTime);
                                    const runCount = result.map(v => parseInt(v.data['runCount']));
                                    const totalRunCount = this.sum(runCount);
                                    const pnjAmount = this.compileAmount(result.map(v => v.data['pnjAmount']));
                                    const totalPnjAmount = this.sum(pnjAmount);
                                    const formatedTotalPnjAmount = this.formatAmount(totalPnjAmount);
                                    const cytizenAmount = this.compileAmount(result.map(v => v.data['cytizenAmount']));
                                    const totalCytizenAmount = this.sum(cytizenAmount);
                                    const formatedTotalCytizenAmount = this.formatAmount(totalCytizenAmount);
                                    const totalAmount = totalPnjAmount + totalCytizenAmount;
                                    const formatedTotalAmount = this.formatAmount(totalAmount);
                                    const embed = new discord_js_1.MessageEmbed();
                                    let driver = this.client.users.cache.find(user => user.id === employee.id);
                                    let roles = message.mentions.members.first().roles.cache;
                                    let rolesList = "";
                                    let i = 0;
                                    roles.map(r => {
                                        i++;
                                        if (r.name != '@everyone') {
                                            if (roles.size - 1 > 1 && i < roles.size - 1) {
                                                rolesList += `${r.name},`;
                                            }
                                            else {
                                                rolesList += `${r.name}`;
                                            }
                                        }
                                    });
                                    embed.setAuthor("Informations chauffeur", driver.displayAvatarURL())
                                        .setDescription(`**Fiche employé:**\n*Nom:* <@!${driver.id}>\n*Grade:* ***${rolesList.replace(',', ' | ')}***\n*Recruté le:* ***${this.formatTimestamp(new Date(Math.min(...timestamps)))}***`)
                                        .setColor(8421504)
                                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();
                                    embed.fields = [
                                        {
                                            'name': `󠀠`,
                                            'value': `󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Periode`,
                                            'value': `Du ${this.formatTimestamp(new Date(Math.min(...timestamps)))} au ${this.formatTimestamp(new Date(Math.max(...timestamps)))}\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Temps de travail`,
                                            'value': `${totalWorkingTime.time.formated}`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Nombre de courses`,
                                            'value': `${totalRunCount}\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses PNJ`,
                                            'value': `${formatedTotalPnjAmount}$`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses citoyennes`,
                                            'value': `${formatedTotalCytizenAmount}$\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Total`,
                                            'value': `${formatedTotalAmount}$\n󠀠`,
                                            'inline': true
                                        },
                                        {
                                            'name': `󠀠`,
                                            'value': `󠀠`,
                                            'inline': false
                                        },
                                    ];
                                    channel.send(embed);
                                }
                            }
                        });
                    }
                    else {
                        if (args.length < 2) {
                            const content = 'Missing arguments';
                            channel.send(content);
                        }
                        else {
                            const content = 'Invalid arguments';
                            channel.send(content);
                        }
                    }
                }
            }
        });
    }
    sum(array) {
        return array.reduce((sum, current) => sum + current, 0);
    }
    formatAmount(amount) {
        let formatedAmount = amount.toString().split('.')[0].split('').reverse().join('');
        let decimal = amount.toString().split('.')[1];
        let splitedAmount = formatedAmount.match(/.{1,3}/g);
        let temp = [];
        splitedAmount.forEach(el => {
            if (el.match(/(?<=^|\s)(?!000(?:\s|$))\S+(?=\s|$)/g)) {
                el = el.split('').reverse().join('');
            }
            temp.push(el);
        });
        if (decimal) {
            return `${temp.reverse().join(' ')}.${amount.toString().split('.')[1]}`;
        }
        else {
            return `${temp.reverse().join(' ')}`;
        }
    }
    compileAmount(array) {
        return array.map(data => parseInt(data.replace(/\s+/g, '')));
    }
    ;
    formatTimestamp(date) {
        let ye = new Intl.DateTimeFormat('fr', { year: 'numeric' }).format(date);
        let mo = new Intl.DateTimeFormat('fr', { month: 'short' }).format(date);
        let da = new Intl.DateTimeFormat('fr', { day: '2-digit' }).format(date);
        return `${da}-${mo}-${ye}`;
    }
    compileWorkingTime(array) {
        const times = array.map(data => data.replace(/[hH]/g, ' '));
        const splitedTimes = times.map(data => data.replace(/[mM]/g, ''));
        const ct = splitedTimes.map(data => data.split(' '));
        const compiledTimes = ct.map(data => {
            return data.map(value => {
                if (!value.match(/\w+$/g)) {
                    return '0';
                }
                return value;
            });
        });
        const timesInMin = compiledTimes.map(data => {
            if (data.length > 1) {
                return (parseInt(data[0]) * 60 + parseInt(data[1]));
            }
            else {
                return parseInt(data[0]);
            }
        });
        let minutes = this.sum(timesInMin) % 60;
        let hours = Math.floor(this.sum(timesInMin) / 60);
        let day = 0;
        if (hours > 24) {
            let d = Math.floor(hours / 24);
            hours = hours % 24;
            day = d;
        }
        let formated = `${hours}h ${minutes}min`;
        if (day > 0) {
            formated = `${day}j ${hours}h ${minutes}min`;
        }
        return {
            totalTime: this.sum(timesInMin),
            time: {
                day: day,
                hours: hours,
                minutes: minutes,
                formated: formated
            }
        };
    }
    ;
}
exports.MessagesListenerService = MessagesListenerService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZXMtbGlzdGVuZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9zZXJ2aWNlcy9tZXNzYWdlcy9tZXNzYWdlcy1saXN0ZW5lci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDJDQUFrRDtBQUVsRCxrREFBa0Q7QUFJbEQsTUFBYSx1QkFBdUI7SUFDaEMsWUFBb0IsTUFBYyxFQUFVLE9BQXNCLEVBQVUsZ0JBQWlDO1FBQXpGLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUFlO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFpQjtRQUN6RyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDO0lBQ25FLENBQUM7SUFFTSxzQkFBc0I7UUFDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxFQUFFO1lBQ2hDLE1BQU0sT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDaEMsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7WUFFbkMsSUFBSSxPQUFPLENBQUMsRUFBRSxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRTtnQkFDeEMsSUFBSSxPQUFPLENBQUMsT0FBTyxLQUFLLElBQUksSUFBSSxPQUFPLENBQUMsT0FBTyxLQUFLLFFBQVEsRUFBRTtvQkFDMUQsTUFBTSxLQUFLLEdBQWlCLElBQUkseUJBQVksRUFBRSxDQUFDO29CQUUvQyxLQUFLLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLDhEQUE4RCxFQUFFLG9CQUFvQixDQUFDO3lCQUNwSCxjQUFjLENBQUMsTUFBTSxRQUFRLEdBQUcsQ0FBQzt5QkFDakMsUUFBUSxDQUFDLEtBQUssQ0FBQzt5QkFDZixTQUFTLENBQUMsZUFBZSxFQUFFLCtFQUErRSxDQUFDO3lCQUMzRyxZQUFZLENBQUMsK0VBQStFLENBQUM7eUJBQzdGLFlBQVksRUFBRSxDQUFDO29CQUVwQixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN2QjtnQkFFRCxJQUFJLE9BQU8sQ0FBQyxPQUFPLEtBQUssSUFBSSxJQUFJLE9BQU8sQ0FBQyxPQUFPLEtBQUssTUFBTSxFQUFFO29CQUN4RCxNQUFNLEtBQUssR0FBaUIsSUFBSSx5QkFBWSxFQUFFLENBQUM7b0JBRS9DLEtBQUssQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsOERBQThELEVBQUUsb0JBQW9CLENBQUM7eUJBQ2xILGNBQWMsQ0FBQyxNQUFNLFFBQVEsR0FBRyxDQUFDO3lCQUNqQyxRQUFRLENBQUMsUUFBUSxDQUFDO3lCQUNsQixTQUFTLENBQUMsZUFBZSxFQUFFLCtFQUErRSxDQUFDO3lCQUMzRyxZQUFZLENBQUMsK0VBQStFLENBQUM7eUJBQzdGLFlBQVksRUFBRSxDQUFDO29CQUVwQixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN2QjthQUNKO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0scUJBQXFCO1FBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsRUFBRTtZQUNoQyxNQUFNLE9BQU8sR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDO1lBQ2hDLE1BQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDO1lBRW5DLElBQUksT0FBTyxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUU7Z0JBQ3ZDLE1BQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUV4QyxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUVoRCxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLFNBQVMsRUFBRTtvQkFDM0MsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTt3QkFDbkIsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLHNEQUFzRCxDQUFDLEVBQUU7NEJBQ3ZFLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO2dDQUNsQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsOEJBQThCLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLDhCQUE4QixDQUFDLEVBQUU7b0NBQ2hHLE1BQU0sYUFBYSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0NBQy9ELE1BQU0sU0FBUyxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0NBRTNELE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQ0FFbEQsTUFBTSxLQUFLLEdBQUcsSUFBSSx5QkFBWSxFQUFFLENBQUMsU0FBUyxDQUFDLG9CQUFvQixFQUFFLDhEQUE4RCxFQUFFLG9CQUFvQixDQUFDO3lDQUNqSixjQUFjLENBQUMsTUFBTSxRQUFRLEdBQUcsQ0FBQzt5Q0FDakMsUUFBUSxDQUFDLE1BQU0sQ0FBQzt5Q0FDaEIsU0FBUyxDQUFDLG1CQUFtQixJQUFJLEVBQUUsRUFBRSwrRUFBK0UsQ0FBQzt5Q0FDckgsWUFBWSxDQUFDLCtFQUErRSxDQUFDO3lDQUM3RixZQUFZLEVBQUUsQ0FBQztvQ0FFcEIsS0FBSyxDQUFDLE1BQU0sR0FBRzt3Q0FDWDs0Q0FDSSxNQUFNLEVBQUUsa0JBQWtCOzRDQUMxQixPQUFPLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUU7NENBQ3JCLFFBQVEsRUFBRSxJQUFJO3lDQUNqQjt3Q0FDRDs0Q0FDSSxNQUFNLEVBQUUsbUJBQW1COzRDQUMzQixPQUFPLEVBQUUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU07NENBQ3pCLFFBQVEsRUFBRSxJQUFJO3lDQUNqQjt3Q0FDRDs0Q0FDSSxNQUFNLEVBQUUsb0JBQW9COzRDQUM1QixPQUFPLEVBQUUsR0FBRyxhQUFhLElBQUk7NENBQzdCLFFBQVEsRUFBRSxLQUFLO3lDQUNsQjt3Q0FDRDs0Q0FDSSxNQUFNLEVBQUUsMkJBQTJCOzRDQUNuQyxPQUFPLEVBQUUsR0FBRyxTQUFTLElBQUk7NENBQ3pCLFFBQVEsRUFBRSxJQUFJO3lDQUNqQjt3Q0FDRDs0Q0FDSSxNQUFNLEVBQUUsT0FBTzs0Q0FDZixPQUFPLEVBQUUsR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTs0Q0FDOUUsUUFBUSxFQUFFLElBQUk7eUNBQ2pCO3dDQUNEOzRDQUNJLE1BQU0sRUFBRSxJQUFJOzRDQUNaLE9BQU8sRUFBRSxJQUFJOzRDQUNiLFFBQVEsRUFBRSxLQUFLO3lDQUNsQjtxQ0FDSixDQUFDO29DQUVGLE1BQU0sR0FBRyxHQUFHLFVBQVUsR0FBRyxJQUFJLEdBQUcsR0FBRyxDQUFDO29DQUVwQyxNQUFNLElBQUksR0FBRzt3Q0FDVCxVQUFVLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dDQUM3QixXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQzt3Q0FDcEIsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7d0NBQ2pCLGFBQWEsRUFBRSxhQUFhO3dDQUM1QixTQUFTLEVBQUUsU0FBUzt3Q0FDcEIsU0FBUyxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUU7d0NBQ3JCLE1BQU0sRUFBRSxDQUFDO3FDQUNaLENBQUE7b0NBRUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7b0NBRXhDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUNBQ3ZCO3FDQUFNO29DQUNILE1BQU0sT0FBTyxHQUFHLHNCQUFzQixDQUFDO29DQUV2QyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lDQUN6Qjs2QkFDSjtpQ0FBTTtnQ0FDSCxNQUFNLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQztnQ0FFcEMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs2QkFDekI7eUJBQ0o7NkJBQU07NEJBQ0gsTUFBTSxPQUFPLEdBQUcscUJBQXFCLENBQUM7NEJBRXRDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ3pCO3FCQUNKO3lCQUFNO3dCQUNILElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ2pCLE1BQU0sT0FBTyxHQUFHLG1CQUFtQixDQUFDOzRCQUVwQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUN6Qjs2QkFBTTs0QkFDSCxNQUFNLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQzs0QkFFcEMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzt5QkFDekI7cUJBQ0o7aUJBQ0o7YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLG9CQUFvQjtRQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLEVBQUU7WUFDaEMsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztZQUVoQyxJQUFJLE9BQU8sQ0FBQyxFQUFFLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUFFO2dCQUN2QyxNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFFeEMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxTQUFTLEVBQUU7b0JBQzNDLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDbkUsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTs0QkFDbkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUscUNBQXFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO2dDQUN6RixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO2dDQUN6RCxNQUFNLEtBQUssR0FBaUIsSUFBSSx5QkFBWSxFQUFFLENBQUM7Z0NBRS9DLEtBQUssQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsOERBQThELEVBQUUsb0JBQW9CLENBQUM7cUNBQ2xILGNBQWMsQ0FBQyxxQ0FBcUMsQ0FBQztxQ0FDckQsUUFBUSxDQUFDLEtBQUssQ0FBQztxQ0FDZixTQUFTLENBQUMsZUFBZSxFQUFFLCtFQUErRSxDQUFDO3FDQUMzRyxZQUFZLENBQUMsK0VBQStFLENBQUM7cUNBQzdGLFlBQVksRUFBRSxDQUFDO2dDQUVwQixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUN4QixDQUFDLENBQUMsQ0FBQzt5QkFDTjt3QkFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOzRCQUNuQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFdBQVcsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0NBQzdELElBQUksS0FBSyxFQUFFO29DQUNQLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEVBQUUsaUJBQWlCLElBQUksQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUU7d0NBQzVILElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdDQUFnQyxDQUFDLENBQUM7d0NBQ3BELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQzt3Q0FFakQsTUFBTSxLQUFLLEdBQWlCLElBQUkseUJBQVksRUFBRSxDQUFDO3dDQUUvQyxLQUFLLENBQUMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLDhEQUE4RCxFQUFFLG9CQUFvQixDQUFDOzZDQUNsSCxjQUFjLENBQUMsZUFBZSxDQUFDOzZDQUMvQixRQUFRLENBQUMsS0FBSyxDQUFDOzZDQUNmLFNBQVMsQ0FBQyxlQUFlLEVBQUUsK0VBQStFLENBQUM7NkNBQzNHLFlBQVksQ0FBQywrRUFBK0UsQ0FBQzs2Q0FDN0YsWUFBWSxFQUFFLENBQUM7d0NBRXBCLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0NBQ3hCLENBQUMsQ0FBQyxDQUFDO2lDQUNOO3FDQUFNO29DQUNILE1BQU0sS0FBSyxHQUFpQixJQUFJLHlCQUFZLEVBQUUsQ0FBQztvQ0FFL0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsMEJBQTBCLElBQUksQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztvQ0FFdEUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSw4REFBOEQsRUFBRSxvQkFBb0IsQ0FBQzt5Q0FDbEgsY0FBYyxDQUFDLGdCQUFnQixJQUFJLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDO3lDQUMzRCxRQUFRLENBQUMsUUFBUSxDQUFDO3lDQUNsQixTQUFTLENBQUMsZUFBZSxFQUFFLCtFQUErRSxDQUFDO3lDQUMzRyxZQUFZLENBQUMsK0VBQStFLENBQUM7eUNBQzdGLFlBQVksRUFBRSxDQUFDO29DQUVwQixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lDQUN2Qjs0QkFDTCxDQUFDLENBQUMsQ0FBQzt5QkFDTjtxQkFDSjt5QkFBTTt3QkFDSCxNQUFNLEtBQUssR0FBaUIsSUFBSSx5QkFBWSxFQUFFLENBQUM7d0JBRS9DLEtBQUssQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsOERBQThELEVBQUUsb0JBQW9CLENBQUM7NkJBQ2xILGNBQWMsQ0FBQyw4REFBOEQsQ0FBQzs2QkFDOUUsUUFBUSxDQUFDLFFBQVEsQ0FBQzs2QkFDbEIsU0FBUyxDQUFDLGVBQWUsRUFBRSwrRUFBK0UsQ0FBQzs2QkFDM0csWUFBWSxDQUFDLCtFQUErRSxDQUFDOzZCQUM3RixZQUFZLEVBQUUsQ0FBQzt3QkFFcEIsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDdkI7aUJBQ0o7YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLHdCQUF3QjtRQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLEVBQUU7WUFDaEMsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztZQUNoQyxNQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUVoRCxJQUFJLE9BQU8sQ0FBQyxFQUFFLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBRSxFQUFFO2dCQUMxQyxNQUFNLElBQUksR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFFeEMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxRQUFRLEVBQUU7b0JBQzFDLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7d0JBQ25CLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFOzRCQUNqRCxJQUFJLElBQUksRUFBRTtnQ0FDTixJQUFJLFlBQVksR0FBRyxFQUFFLENBQUM7Z0NBRXRCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQ0FDL0MsWUFBWSxDQUFDLElBQUksQ0FBQzt3Q0FDZCxHQUFHLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0NBQ3pCLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztxQ0FDL0IsQ0FBQyxDQUFDO2lDQUNOO2dDQUVELE1BQU0sTUFBTSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksUUFBUSxDQUFDLEVBQUUsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBRTdILElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0NBQ25CLE1BQU0sVUFBVSxHQUFhLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0NBRWxFLE1BQU0sV0FBVyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7b0NBQzNELE1BQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxDQUFDO29DQUU5RCxNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO29DQUMvRCxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29DQUV6QyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQ0FDM0UsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztvQ0FDM0MsTUFBTSxzQkFBc0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29DQUVqRSxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQ0FDbkYsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO29DQUNuRCxNQUFNLDBCQUEwQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsQ0FBQztvQ0FFekUsTUFBTSxXQUFXLEdBQUcsY0FBYyxHQUFHLGtCQUFrQixDQUFDO29DQUN4RCxNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7b0NBRTNELE1BQU0sS0FBSyxHQUFpQixJQUFJLHlCQUFZLEVBQUUsQ0FBQztvQ0FFL0MsS0FBSyxDQUFDLFNBQVMsQ0FBQywyQkFBMkIsRUFBRSw4REFBOEQsRUFBRSxvQkFBb0IsQ0FBQzt5Q0FDN0gsY0FBYyxDQUFDLGVBQWUsUUFBUSxDQUFDLEVBQUUsR0FBRyxDQUFDO3lDQUM3QyxRQUFRLENBQUMsT0FBTyxDQUFDO3lDQUNqQixTQUFTLENBQUMsZUFBZSxFQUFFLCtFQUErRSxDQUFDO3lDQUMzRyxZQUFZLENBQUMsK0VBQStFLENBQUM7eUNBQzdGLFlBQVksRUFBRSxDQUFDO29DQUVwQixLQUFLLENBQUMsTUFBTSxHQUFHO3dDQUNYOzRDQUNJLE1BQU0sRUFBRSxTQUFTOzRDQUNqQixPQUFPLEVBQUUsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxNQUFNOzRDQUMxSSxRQUFRLEVBQUUsS0FBSzt5Q0FDbEI7d0NBQ0Q7NENBQ0ksTUFBTSxFQUFFLGtCQUFrQjs0Q0FDMUIsT0FBTyxFQUFFLEdBQUcsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTs0Q0FDNUMsUUFBUSxFQUFFLEtBQUs7eUNBQ2xCO3dDQUNEOzRDQUNJLE1BQU0sRUFBRSxtQkFBbUI7NENBQzNCLE9BQU8sRUFBRSxHQUFHLGFBQWEsTUFBTTs0Q0FDL0IsUUFBUSxFQUFFLEtBQUs7eUNBQ2xCO3dDQUNEOzRDQUNJLE1BQU0sRUFBRSxhQUFhOzRDQUVyQixPQUFPLEVBQUUsR0FBRyxzQkFBc0IsR0FBRzs0Q0FDckMsUUFBUSxFQUFFLEtBQUs7eUNBQ2xCO3dDQUNEOzRDQUNJLE1BQU0sRUFBRSxvQkFBb0I7NENBQzVCLE9BQU8sRUFBRSxHQUFHLDBCQUEwQixPQUFPOzRDQUM3QyxRQUFRLEVBQUUsS0FBSzt5Q0FDbEI7d0NBQ0Q7NENBQ0ksTUFBTSxFQUFFLE9BQU87NENBQ2YsT0FBTyxFQUFFLEdBQUcsbUJBQW1CLE9BQU87NENBQ3RDLFFBQVEsRUFBRSxJQUFJO3lDQUNqQjt3Q0FDRDs0Q0FDSSxNQUFNLEVBQUUsV0FBVyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUk7NENBQzlCLE9BQU8sRUFBRSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLFNBQVM7NENBQy9FLFFBQVEsRUFBRSxJQUFJO3lDQUNqQjtxQ0FDSixDQUFDO29DQUVGLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dDQUMzQixJQUFJOzRDQUNBLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7NENBQ2hCLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7NENBRWYsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLElBQUksSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxHQUFHLENBQUMsRUFDM0csRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRTtnREFDdEMsSUFBSSxRQUFRLEdBQUcsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO2dEQUVqQyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksSUFBSSxFQUFFO29EQUM1QyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLENBQUM7b0RBRWxELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdEQUFnRCxDQUFDLENBQUM7b0RBQ3BFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO3dEQUNkLE1BQU0sRUFBRSxXQUFXO3dEQUNuQixPQUFPLEVBQUUsSUFBSTt3REFDYixTQUFTLEVBQUUsR0FBRyxDQUFDLEVBQUU7cURBQ3BCLENBQUMsQ0FBQztvREFFSCxJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztvREFFdkMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRSw4REFBOEQsRUFBRSxvQkFBb0IsQ0FBQzt5REFDckgsY0FBYyxDQUFDLGVBQWUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUM7eURBQ25FLFFBQVEsQ0FBQyxRQUFRLENBQUM7eURBQ2xCLFNBQVMsQ0FBQyxtQkFBbUIsSUFBSSxFQUFFLEVBQUUsK0VBQStFLENBQUMsQ0FBQztvREFFM0gsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTt3REFDcEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxXQUFXLE1BQU0sQ0FBQyxHQUFHLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztvREFDcEUsQ0FBQyxDQUFDLENBQUM7b0RBRUgsTUFBTSxHQUFHLEdBQUcsVUFBVSxHQUFHLElBQUksR0FBRyxHQUFHLENBQUM7b0RBRXBDLE1BQU0sSUFBSSxHQUFHO3dEQUNULFVBQVUsRUFBRSxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFO3dEQUM3QyxXQUFXLEVBQUUsZ0JBQWdCO3dEQUM3QixRQUFRLEVBQUUsYUFBYTt3REFDdkIsYUFBYSxFQUFFLGtCQUFrQjt3REFDakMsU0FBUyxFQUFFLGNBQWM7d0RBQ3pCLFdBQVcsRUFBRSxXQUFXO3dEQUN4QixVQUFVLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3REFDN0IsS0FBSyxFQUFFLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUc7d0RBQzlDLFNBQVMsRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFO3FEQUN4QixDQUFBO29EQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO29EQUV4QyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lEQUNuQjtxREFBTTtvREFDSCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO29EQUNuRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQzt3REFDZCxNQUFNLEVBQUUsVUFBVTt3REFDbEIsU0FBUyxFQUFFLEdBQUcsQ0FBQyxFQUFFO3FEQUNwQixDQUFDLENBQUM7b0RBRUgsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7b0RBRXZDLEtBQUssQ0FBQyxTQUFTLENBQUMseUJBQXlCLEVBQUUsOERBQThELEVBQUUsb0JBQW9CLENBQUM7eURBQzNILFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztvREFFeEIsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpREFDbkI7Z0RBRUQsR0FBRyxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLDZCQUE2QixFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7NENBQ2xHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO2dEQUNoQixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyx5REFBeUQsQ0FBQyxDQUFDO2dEQUM5RSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztvREFDZixNQUFNLEVBQUUsV0FBVztvREFDbkIsU0FBUyxFQUFFLEdBQUcsQ0FBQyxFQUFFO2lEQUNwQixDQUFDLENBQUM7Z0RBRUgsSUFBSSxLQUFLLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztnREFFMUIsS0FBSyxDQUFDLFNBQVMsQ0FBQyxpQ0FBaUMsRUFBRSw4REFBOEQsRUFBRSxvQkFBb0IsQ0FBQztxREFDbkksUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dEQUV4QixHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dEQUVoQixHQUFHLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQzs0Q0FDbEcsQ0FBQyxDQUFDLENBQUM7eUNBQ1Y7d0NBQUMsT0FBTyxLQUFLLEVBQUU7NENBQ1osT0FBTyxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLENBQUMsQ0FBQzt5Q0FDMUM7b0NBQ0wsQ0FBQyxDQUFDLENBQUM7aUNBQ047cUNBQU07b0NBQ0gsTUFBTSxLQUFLLEdBQWlCLElBQUkseUJBQVksRUFBRSxDQUFDO29DQUUvQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO29DQUN6RCxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQzt3Q0FDZixNQUFNLEVBQUUsV0FBVzt3Q0FDbkIsU0FBUyxFQUFFLE9BQU8sQ0FBQyxFQUFFO3FDQUN4QixDQUFDLENBQUM7b0NBRUgsS0FBSyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSw4REFBOEQsRUFBRSxvQkFBb0IsQ0FBQzt5Q0FDbEgsY0FBYyxDQUFDLHVDQUF1QyxRQUFRLENBQUMsRUFBRSx1QkFBdUIsQ0FBQzt5Q0FDekYsUUFBUSxDQUFDLFFBQVEsQ0FBQzt5Q0FDbEIsU0FBUyxDQUFDLGVBQWUsRUFBRSwrRUFBK0UsQ0FBQzt5Q0FDM0csWUFBWSxDQUFDLCtFQUErRSxDQUFDO3lDQUM3RixZQUFZLEVBQUUsQ0FBQztvQ0FFcEIsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQ0FDdkI7NkJBQ0o7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7cUJBQ047eUJBQU07d0JBQ0gsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDakIsTUFBTSxPQUFPLEdBQUcsbUJBQW1CLENBQUM7NEJBRXBDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ3pCOzZCQUFNOzRCQUNILE1BQU0sT0FBTyxHQUFHLG1CQUFtQixDQUFDOzRCQUVwQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUN6QjtxQkFDSjtpQkFDSjthQXVHSjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLHVCQUF1QjtRQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLEVBQUU7WUFDaEMsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQztZQUNoQyxNQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUVoRCxJQUFJLE9BQU8sQ0FBQyxFQUFFLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2pELE1BQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUV4QyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLE9BQU8sRUFBRTtvQkFDekMsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTt3QkFDbkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7NEJBQ2pELElBQUksSUFBSSxFQUFFO2dDQUNOLElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQztnQ0FFdEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29DQUMvQyxZQUFZLENBQUMsSUFBSSxDQUFDO3dDQUNkLEdBQUcsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3Q0FDekIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3FDQUMvQixDQUFDLENBQUM7aUNBQ047Z0NBRUQsTUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQ0FFN0YsSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQ0FDbkIsTUFBTSxVQUFVLEdBQWEsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztvQ0FFbEUsTUFBTSxXQUFXLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztvQ0FDM0QsTUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLENBQUM7b0NBRTlELE1BQU0sUUFBUSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7b0NBQy9ELE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7b0NBRXpDLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO29DQUMzRSxNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29DQUMzQyxNQUFNLHNCQUFzQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUM7b0NBRWpFLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO29DQUNuRixNQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7b0NBQ25ELE1BQU0sMEJBQTBCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29DQUV6RSxNQUFNLFdBQVcsR0FBRyxjQUFjLEdBQUcsa0JBQWtCLENBQUM7b0NBQ3hELE1BQU0sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQ0FFM0QsTUFBTSxLQUFLLEdBQWlCLElBQUkseUJBQVksRUFBRSxDQUFDO29DQUUvQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7b0NBRTNFLElBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7b0NBQ3pELElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztvQ0FFbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29DQUNWLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0NBQ1YsQ0FBQyxFQUFFLENBQUM7d0NBQ0osSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLFdBQVcsRUFBRTs0Q0FDdkIsSUFBSSxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO2dEQUMxQyxTQUFTLElBQUksR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUM7NkNBQzdCO2lEQUFNO2dEQUNILFNBQVMsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs2Q0FDNUI7eUNBQ0o7b0NBQ0wsQ0FBQyxDQUFDLENBQUM7b0NBRUgsS0FBSyxDQUFDLFNBQVMsQ0FBQyx3QkFBd0IsRUFBRSxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzt5Q0FDL0QsY0FBYyxDQUFDLGlDQUFpQyxNQUFNLENBQUMsRUFBRSxrQkFBa0IsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLHlCQUF5QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzt5Q0FDOUwsUUFBUSxDQUFDLE9BQU8sQ0FBQzt5Q0FDakIsU0FBUyxDQUFDLGVBQWUsRUFBRSwrRUFBK0UsQ0FBQzt5Q0FDM0csWUFBWSxDQUFDLCtFQUErRSxDQUFDO3lDQUM3RixZQUFZLEVBQUUsQ0FBQztvQ0FFcEIsS0FBSyxDQUFDLE1BQU0sR0FBRzt3Q0FDWDs0Q0FDSSxNQUFNLEVBQUUsSUFBSTs0Q0FDWixPQUFPLEVBQUUsSUFBSTs0Q0FDYixRQUFRLEVBQUUsS0FBSzt5Q0FDbEI7d0NBQ0Q7NENBQ0ksTUFBTSxFQUFFLFNBQVM7NENBQ2pCLE9BQU8sRUFBRSxNQUFNLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLE1BQU07NENBQzFJLFFBQVEsRUFBRSxLQUFLO3lDQUNsQjt3Q0FDRDs0Q0FDSSxNQUFNLEVBQUUsa0JBQWtCOzRDQUMxQixPQUFPLEVBQUUsR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFOzRDQUM1QyxRQUFRLEVBQUUsS0FBSzt5Q0FDbEI7d0NBQ0Q7NENBQ0ksTUFBTSxFQUFFLG1CQUFtQjs0Q0FDM0IsT0FBTyxFQUFFLEdBQUcsYUFBYSxNQUFNOzRDQUMvQixRQUFRLEVBQUUsS0FBSzt5Q0FDbEI7d0NBQ0Q7NENBQ0ksTUFBTSxFQUFFLGFBQWE7NENBRXJCLE9BQU8sRUFBRSxHQUFHLHNCQUFzQixHQUFHOzRDQUNyQyxRQUFRLEVBQUUsS0FBSzt5Q0FDbEI7d0NBQ0Q7NENBQ0ksTUFBTSxFQUFFLG9CQUFvQjs0Q0FDNUIsT0FBTyxFQUFFLEdBQUcsMEJBQTBCLE9BQU87NENBQzdDLFFBQVEsRUFBRSxLQUFLO3lDQUNsQjt3Q0FDRDs0Q0FDSSxNQUFNLEVBQUUsT0FBTzs0Q0FDZixPQUFPLEVBQUUsR0FBRyxtQkFBbUIsT0FBTzs0Q0FDdEMsUUFBUSxFQUFFLElBQUk7eUNBQ2pCO3dDQU1EOzRDQUNJLE1BQU0sRUFBRSxJQUFJOzRDQUNaLE9BQU8sRUFBRSxJQUFJOzRDQUNiLFFBQVEsRUFBRSxLQUFLO3lDQUNsQjtxQ0FDSixDQUFDO29DQUVGLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUNBQ3ZCOzZCQUNKO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO3lCQUFNO3dCQUNILElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ2pCLE1BQU0sT0FBTyxHQUFHLG1CQUFtQixDQUFDOzRCQUVwQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUN6Qjs2QkFBTTs0QkFDSCxNQUFNLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQzs0QkFFcEMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzt5QkFDekI7cUJBQ0o7aUJBQ0o7YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLEdBQUcsQ0FBQyxLQUFvQjtRQUM1QixPQUFPLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxHQUFHLEdBQUcsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFTyxZQUFZLENBQUMsTUFBTTtRQUN2QixJQUFJLGNBQWMsR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbEYsSUFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUU5QyxJQUFJLGFBQWEsR0FBRyxjQUFjLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXBELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUVkLGFBQWEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLHNDQUFzQyxDQUFDLEVBQUU7Z0JBQ2xELEVBQUUsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN4QztZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLE9BQU8sRUFBRTtZQUVULE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUMzRTthQUFNO1lBRUgsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQztTQUN4QztJQUNMLENBQUM7SUFFTyxhQUFhLENBQUMsS0FBSztRQUN2QixPQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2pFLENBQUM7SUFBQSxDQUFDO0lBRU0sZUFBZSxDQUFDLElBQUk7UUFDeEIsSUFBSSxFQUFFLEdBQUcsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6RSxJQUFJLEVBQUUsR0FBRyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hFLElBQUksRUFBRSxHQUFHLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFeEUsT0FBTyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVPLGtCQUFrQixDQUFDLEtBQUs7UUFDNUIsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDNUQsTUFBTSxZQUFZLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFbEUsTUFBTSxFQUFFLEdBQUcsWUFBWSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNyRCxNQUFNLGFBQWEsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2hDLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ3ZCLE9BQU8sR0FBRyxDQUFDO2lCQUNkO2dCQUVELE9BQU8sS0FBSyxDQUFDO1lBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLFVBQVUsR0FBRyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3hDLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2pCLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2FBQ3REO2lCQUFNO2dCQUNILE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzVCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUN4QyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDbEQsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBRVosSUFBSSxLQUFLLEdBQUcsRUFBRSxFQUFFO1lBQ1osSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDL0IsS0FBSyxHQUFHLEtBQUssR0FBRyxFQUFFLENBQUE7WUFDbEIsR0FBRyxHQUFHLENBQUMsQ0FBQztTQUNYO1FBRUQsSUFBSSxRQUFRLEdBQUcsR0FBRyxLQUFLLEtBQUssT0FBTyxLQUFLLENBQUM7UUFFekMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFO1lBQ1QsUUFBUSxHQUFHLEdBQUcsR0FBRyxLQUFLLEtBQUssS0FBSyxPQUFPLEtBQUssQ0FBQztTQUNoRDtRQUVELE9BQU87WUFDSCxTQUFTLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUM7WUFDL0IsSUFBSSxFQUFFO2dCQUNGLEdBQUcsRUFBRSxHQUFHO2dCQUNSLEtBQUssRUFBRSxLQUFLO2dCQUNaLE9BQU8sRUFBRSxPQUFPO2dCQUNoQixRQUFRLEVBQUUsUUFBUTthQUNyQjtTQUNKLENBQUM7SUFDTixDQUFDO0lBQUEsQ0FBQztDQUNMO0FBenZCRCwwREF5dkJDIn0=