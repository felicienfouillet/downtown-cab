"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReactionsListenerService = void 0;
class ReactionsListenerService {
    constructor(client, _logger) {
        this.client = client;
        this._logger = _logger;
        this._logger.info('[Downtown Cab.] Reactions listener loaded.');
    }
    ;
    listenValidationsMessages() {
        this.client.on('messageReactionAdd', (reaction, user) => {
        });
    }
}
exports.ReactionsListenerService = ReactionsListenerService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVhY3Rpb25zLWxpc3RlbmVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvc2VydmljZXMvcmVhY3Rpb25zL3JlYWN0aW9ucy1saXN0ZW5lci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUlBLE1BQWEsd0JBQXdCO0lBRWpDLFlBQW9CLE1BQWMsRUFBVSxPQUFzQjtRQUE5QyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsWUFBTyxHQUFQLE9BQU8sQ0FBZTtRQUM5RCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyw0Q0FBNEMsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFBQSxDQUFDO0lBRUsseUJBQXlCO1FBQzVCLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLG9CQUFvQixFQUFFLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxFQUFFO1FBRXhELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztDQUNKO0FBWEQsNERBV0MifQ==