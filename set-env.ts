const fs = require('fs');

fs.readFile('./src/env/environment.prod.json', 'utf8', (err, res) => {
    if (err) {
        console.error(err);
    }

    fs.writeFile('./dist/env/environment.json', res, 'utf8', err => {
        if (err) {
            console.error(err);
        }
    });
})

