import { FirebaseConfig } from "../../models/firebase-config.model";

export const config: FirebaseConfig = {
    apiKey: "AIzaSyBB_UPIhi98IlnijCYUW4yi6aPW7QexC5s",
    authDomain: "downtown-cab-back.firebaseapp.com",
    databaseURL: "https://downtown-cab-back-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "downtown-cab-back",
    storageBucket: "downtown-cab-back.appspot.com",
    messagingSenderId: "91592984269",
    appId: "1:91592984269:web:84d10065858c3ffba9cb84"
  }