import { Client } from "discord.js";
import DataBaseService from './services/database/database.service';
import * as env from './env/environment.json';
import { MessagesListenerService } from "./services/messages/messages-listener.service";
import { MentionsListenerService } from "./services/mentions/mentions-listener.service";
import { LoggerService } from "./services/logger/logger.service";
import { ReactionsListenerService } from "./services/reactions/reactions-listener.service";

const client: Client = new Client();
const _logger = new LoggerService();

let _databaseService = new DataBaseService(_logger);

let _messagesListener = new MessagesListenerService(client, _logger, _databaseService);
let _mentionsListener = new MentionsListenerService(client, _logger);
let _reactionsListener = new ReactionsListenerService(client, _logger);

client.on('ready', function () {
    _logger.info('[Downtown Cab.] Downtown Cab. discord bot ready.');
});

_messagesListener.listenServicesMessages();
_messagesListener.listenReportsMessages();
_messagesListener.listenDeleteMessages();
_messagesListener.listenAccountingMessages();
_messagesListener.listenManagmentMessages();

_mentionsListener.listenBotMentions();

_reactionsListener.listenValidationsMessages();

client.login(env.token);
