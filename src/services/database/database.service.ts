import firebase from "firebase";
import { config } from '../../env/firebase/firebase.config';
import { LoggerService } from "../logger/logger.service";

export default class DataBaseService {
    public database;

    constructor(private _logger: LoggerService) {
        this.init();

        this._logger.info('[Downtown Cab.] Database service initialized.');
    };

    private init(): void {
        firebase.initializeApp(config);

        this.database = firebase.database();

        this._logger.info('[Firebase] App initialized.');
    }

    public async setOne(ref, data): Promise<any> {
        return firebase.database().ref(ref).set(data);
    }

    public getOne(ref): Promise<any> {
        return firebase.database().ref(ref).once('value').then(snapshot => {
            return snapshot.val();
        });
    }

    public getAll(childRef): Promise<any> {
        return firebase.database().ref(childRef).once('value').then(snapshot => {
            return snapshot.val();
        });
    }

    public deleteOne(ref, callbackMessage): Promise<any> {
        return this.setOne(ref, null).then(() => {
            return callbackMessage;
        });
    }

    public deleteAll(childRef, callbackMessage): Promise<any> {
        return this.setOne(childRef, null).then(() => {
            return callbackMessage;
        });
    }

    public generateUUID(): string {
        return '_' + Math.random().toString(36).substr(2, 9);
    }
}