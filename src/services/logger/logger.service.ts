import * as env from '../../env/environment.json';

export class LoggerService {
    constructor() { }

    public log(message: any): void {
        if (!env.prod) {
            console.log(message);
        }
    }

    public info(message: any): void {
        console.info(message);
    }

    public error(message: any): void {
        console.error(message);
    }
}