import { Client, MessageEmbed } from "discord.js";
import * as env from '../../env/environment.json';
import { LoggerService } from "../logger/logger.service";

export class MentionsListenerService {
    constructor(private client: Client, private _logger: LoggerService) {
        this._logger.info('[Downtown Cab.] Mentions listener loaded.');
    }

    public listenBotMentions(): void {
        this.client.on('message', message => {
            const channel = message.channel;

            if (channel.id == env.channels.help.id) {
                if (message.mentions.has(env.botId)) {
                    const embed: MessageEmbed = new MessageEmbed();

                    embed.setTitle("Help center !")
                        .setAuthor("Liste des commandes", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                        .setColor(0)
                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setTimestamp();

                    embed.fields = [
                        {
                            'name': `󠀠`,
                            'value': `󠀠`,
                            'inline': false
                        },
                        {
                            'name': `Afficher la liste des commandes`,
                            'value': `Mentionner simplement le client **Downtown Cab.**\n󠀠`,
                            'inline': false
                        },
                        // {
                        //     'name': `Demander un rôle`,
                        //     'value': `***!dr @[rôle]*** ou ***!drole @[rôle]***\n󠀠`,
                        //     'inline': false
                        // },
                        {
                            'name': `Signaler une prise de service`,
                            'value': `***!start*** ou ***!s***\n󠀠`,
                            'inline': false
                        },
                        {
                            'name': `Signaler une fin de service`,
                            'value': `***!end*** ou ***!e***\n󠀠`,
                            'inline': false
                        },
                        {
                            'name': `Faire un rapport de service`,
                            'value': `***!report*** [Temps de service -> ***...h...m*** | ***..H...M***]\n [Nombre de courses citoyennes -> ***...***]\n [Montant des courses citoyennes -> ***...***]\n [Montant des courses PNJ (sans les pourboires) -> ***...***]\n\n ou ***!r*** [...]`,
                            'inline': false
                        },
                        {
                            'name': `󠀠`,
                            'value': `󠀠`,
                            'inline': false
                        }
                    ];

                    let driver = this.client.users.cache.find(user => user.id === message.author.id);

                    let roles = message.mentions.members.first().roles.cache;
                    let rolesList = [];

                    roles.map(r => {
                        if (r.name != '@everyone') {
                            rolesList.push(`${r.id}`);
                        }
                    });
                    
                    if (rolesList.filter(r => (driver.id == env.roles.director || r.id == env.roles.coDirector || r.id == env.roles.manager)).length > 0){
                        embed.fields[embed.fields.length - 1] = {
                            'name': `󠀠\n\nCalculer une prime`,
                            'value': `󠀠!p @ [pseudo_discord_chauffeur] [pourcentage_de_prime]\nou !prime @ [pseudo_discord_chauffeur] [pourcentage_de_prime]`,
                            'inline': false
                        };

                        embed.fields.push({
                            'name': `󠀠Informations chauffeur`,
                            'value': `󠀠!i @ [pseudo_discord_chauffeur]\nou !info @ [pseudo_discord_chauffeur]`,
                            'inline': false
                        });

                        embed.fields.push({
                            'name': `󠀠`,
                            'value': `󠀠`,
                            'inline': false
                        });
                    }
                    
                    channel.send(embed);
                }
            }
        });
    }
}