import { Client, MessageEmbed } from "discord.js";
import { collapseTextChangeRangesAcrossMultipleVersions, isRegularExpressionLiteral } from "typescript";
import * as env from '../../env/environment.json';
import DataBaseService from "../database/database.service";
import { LoggerService } from "../logger/logger.service";

export class MessagesListenerService {
    constructor(private client: Client, private _logger: LoggerService, private _databaseService: DataBaseService) {
        this._logger.info('[Downtown Cab.] Messages listener loaded.');
    }

    public listenServicesMessages(): void {
        this.client.on('message', message => {
            const channel = message.channel;
            const authorId = message.author.id;

            if (channel.id == env.channels.services.id) {
                if (message.content === '!s' || message.content === '!start') {
                    const embed: MessageEmbed = new MessageEmbed();

                    embed.setAuthor("Prise de service", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                        .setDescription(`<@!${authorId}>`)
                        .setColor(65280)
                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setTimestamp();

                    channel.send(embed);
                }

                if (message.content === '!e' || message.content === '!end') {
                    const embed: MessageEmbed = new MessageEmbed();

                    embed.setAuthor("Fin de service", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                        .setDescription(`<@!${authorId}>`)
                        .setColor(16711680)
                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                        .setTimestamp();

                    channel.send(embed);
                }
            }
        });
    }

    public listenReportsMessages(): void {
        this.client.on('message', message => {
            const channel = message.channel;
            const authorId = message.author.id;

            if (channel.id == env.channels.reports.id) {
                const args = message.content.split(' ');

                const amountFormat = Intl.NumberFormat('fr-FR');

                if (args[0] === '!r' || args[0] === '!report') {
                    if (args.length === 5) {
                        if (args[1].match(/^(?=\d+[hmHM])((\d+[hH])?(?!\d)?(\d+[mM])?(?!\d))?$/g)) {
                            if (args[2].match(/^(?=\d)(\d*)?$/g)) {
                                if (args[3].match(/^[0-9]{0,9}(\.[0-9]{1,2})?$/g) && args[4].match(/^[0-9]{0,9}(\.[0-9]{1,2})?$/g)) {
                                    const cytizenAmount = amountFormat.format(parseFloat(args[3]));
                                    const pnjAmount = amountFormat.format(parseFloat(args[4]));

                                    const uuid = this._databaseService.generateUUID();

                                    const embed = new MessageEmbed().setAuthor("Rapport de service", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                        .setDescription(`<@!${authorId}>`)
                                        .setColor(131586)
                                        .setFooter(`Downtown Cab. • ${uuid}`, "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();

                                    embed.fields = [
                                        {
                                            'name': `Temps de travail`,
                                            'value': `${args[1]}`,
                                            'inline': true
                                        },
                                        {
                                            'name': `Nombre de courses`,
                                            'value': `${args[2]}\n󠀠`,
                                            'inline': true
                                        },
                                        {
                                            'name': `Courses citoyennes`,
                                            'value': `${cytizenAmount} $`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses PNJ            󠀠`,
                                            'value': `${pnjAmount} $`,
                                            'inline': true
                                        },
                                        {
                                            'name': `Total`,
                                            'value': `${amountFormat.format(parseFloat(args[3]) + parseFloat(args[4]))} $`,
                                            'inline': true
                                        },
                                        {
                                            'name': `󠀠`,
                                            'value': `󠀠`,
                                            'inline': false
                                        }
                                    ];

                                    const ref = 'reports/' + uuid + '/';

                                    const data = {
                                        employeeId: message.author.id,
                                        workingTime: args[1],
                                        runCount: args[2],
                                        cytizenAmount: cytizenAmount,
                                        pnjAmount: pnjAmount,
                                        timestamp: Date.now(),
                                        status: 0
                                    }

                                    this._databaseService.setOne(ref, data);

                                    channel.send(embed);
                                } else {
                                    const content = 'Invalid amount count';

                                    channel.send(content);
                                }
                            } else {
                                const content = 'Invalid run count';

                                channel.send(content);
                            }
                        } else {
                            const content = 'Invalid time format';

                            channel.send(content);
                        }
                    } else {
                        if (args.length < 5) {
                            const content = 'Missing arguments';

                            channel.send(content);
                        } else {
                            const content = 'Invalid arguments';

                            channel.send(content);
                        }
                    }
                }
            }
        });
    }

    public listenDeleteMessages(): void {
        this.client.on('message', message => {
            const channel = message.channel;

            if (channel.id == env.channels.reports.id) {
                const args = message.content.split(' ');

                if (args[0] === '!d' || args[0] === '!delete') {
                    if (message.member.roles.cache.find(r => r.id === env.roles.director)) {
                        if (args.length === 1) {
                            this._databaseService.deleteAll('reports/', `Tous les rapports ont été supprimés`).then(() => {
                                this._logger.info('[Downtown Cab.] Delete all reports.');
                                const embed: MessageEmbed = new MessageEmbed();

                                embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                    .setDescription(`Tous les rapports ont été supprimés`)
                                    .setColor(65280)
                                    .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                    .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                    .setTimestamp();

                                channel.send(embed);
                            });
                        }

                        if (args.length === 2) {
                            this._databaseService.getOne(`reports/${args[1]}/`).then(value => {
                                if (value) {
                                    this._databaseService.deleteOne('reports/' + args[1] + '/', `Le rapports **${args[1]}** a été supprimé`).then(callbackMessage => {
                                        this._logger.info('[Downtown Cab.] Delete report:');
                                        this._logger.info({ id: args[1], value: value });

                                        const embed: MessageEmbed = new MessageEmbed();

                                        embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                            .setDescription(callbackMessage)
                                            .setColor(65280)
                                            .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                            .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                            .setTimestamp();

                                        channel.send(embed);
                                    });
                                } else {
                                    const embed: MessageEmbed = new MessageEmbed();

                                    this._logger.error(`[Downtown Cab.] Report ${args[1]} was not found`);

                                    embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                        .setDescription(`Le rapport **${args[1]}** est introuvable`)
                                        .setColor(16711680)
                                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();

                                    channel.send(embed);
                                }
                            });
                        }
                    } else {
                        const embed: MessageEmbed = new MessageEmbed();

                        embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                            .setDescription(`Vous n'avez pas la permission pour utiliser cette commande !`)
                            .setColor(16711680)
                            .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                            .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                            .setTimestamp();

                        channel.send(embed);
                    }
                }
            }
        });
    }

    public listenAccountingMessages(): void {
        this.client.on('message', message => {
            const channel = message.channel;
            const employee = message.mentions.users.first();

            if (channel.id == env.channels.accounting.id) {
                const args = message.content.split(' ');

                if (args[0] === '!p' || args[0] === '!prime') {
                    if (args.length === 3) {
                        this._databaseService.getAll('reports/').then(data => {
                            if (data) {
                                let compiledData = [];

                                for (let i = 0; i < Object.keys(data).length; i++) {
                                    compiledData.push({
                                        key: Object.keys(data)[i],
                                        data: Object.values(data)[i]
                                    });
                                }

                                const result = compiledData.filter(elements => (elements.data['employeeId'] == employee.id && elements.data['status'] == 0));

                                if (result.length > 0) {
                                    const timestamps: number[] = result.map(v => v.data['timestamp']);

                                    const workingTime = result.map(v => v.data['workingTime']);
                                    const totalWorkingTime = this.compileWorkingTime(workingTime);

                                    const runCount = result.map(v => parseInt(v.data['runCount']));
                                    const totalRunCount = this.sum(runCount);

                                    const pnjAmount = this.compileAmount(result.map(v => v.data['pnjAmount']));
                                    const totalPnjAmount = this.sum(pnjAmount);
                                    const formatedTotalPnjAmount = this.formatAmount(totalPnjAmount);

                                    const cytizenAmount = this.compileAmount(result.map(v => v.data['cytizenAmount']));
                                    const totalCytizenAmount = this.sum(cytizenAmount);
                                    const formatedTotalCytizenAmount = this.formatAmount(totalCytizenAmount);

                                    const totalAmount = totalPnjAmount + totalCytizenAmount;
                                    const formatedTotalAmount = this.formatAmount(totalAmount);

                                    const embed: MessageEmbed = new MessageEmbed();

                                    embed.setAuthor("Prévisualisation de prime", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                        .setDescription(`Prime de <@!${employee.id}>`)
                                        .setColor(8421504)
                                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();

                                    embed.fields = [
                                        {
                                            'name': `Periode`,
                                            'value': `Du ${this.formatTimestamp(new Date(Math.min(...timestamps)))} au ${this.formatTimestamp(new Date(Math.max(...timestamps)))}\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Temps de travail`,
                                            'value': `${totalWorkingTime.time.formated}`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Nombre de courses`,
                                            'value': `${totalRunCount}\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses PNJ`,

                                            'value': `${formatedTotalPnjAmount}$`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses citoyennes`,
                                            'value': `${formatedTotalCytizenAmount}$\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Total`,
                                            'value': `${formatedTotalAmount}$\n󠀠`,
                                            'inline': true
                                        },
                                        {
                                            'name': `Primes (${args[2]}%)`,
                                            'value': `${this.formatAmount((totalAmount * parseInt(args[2])) / 100)}$\n\n󠀠`,
                                            'inline': true
                                        }
                                    ];

                                    channel.send(embed).then(msg => {
                                        try {
                                            msg.react('✔️');
                                            msg.react('❌');

                                            msg.awaitReactions((reaction, user) => !user.bot && (reaction.emoji.name == '✔️' || reaction.emoji.name == '❌'),
                                                { max: 1, time: 15000 }).then(collected => {
                                                    let reaction = collected.first();

                                                    if (reaction.users.reaction.emoji.name == '✔️') {
                                                        const uuid = this._databaseService.generateUUID();

                                                        this._logger.info('[Downtown Cab.] Reaction validation validated:');
                                                        this._logger.info({
                                                            reason: 'Validated',
                                                            bonusId: uuid,
                                                            messageId: msg.id
                                                        });

                                                        let embed = reaction.message.embeds[0];

                                                        embed.setAuthor('Prime enregistrée', 'https://yagami.xyz/content/uploads/2018/11/discord-512-1.png', 'https://yagami.xyz')
                                                            .setDescription(`Prime de <@!${message.mentions.users.first().id}>`)
                                                            .setColor(16754176)
                                                            .setFooter(`Downtown Cab. • ${uuid}`, "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png");

                                                        result.forEach(report => {
                                                            this._databaseService.setOne(`reports/${report.key}/status`, 1);
                                                        });

                                                        const ref = 'bonuses/' + uuid + '/';

                                                        const data = {
                                                            employeeId: message.mentions.users.first().id,
                                                            workingTime: totalWorkingTime,
                                                            runCount: totalRunCount,
                                                            cytizenAmount: totalCytizenAmount,
                                                            pnjAmount: totalPnjAmount,
                                                            totalAmount: totalAmount,
                                                            percentage: parseInt(args[2]),
                                                            prime: (totalAmount * parseInt(args[2])) / 100,
                                                            timestamp: Date.now(),
                                                        }

                                                        this._databaseService.setOne(ref, data);

                                                        msg.edit(embed);
                                                    } else {
                                                        this._logger.info('[Downtown Cab.] Reaction validation canceled:');
                                                        this._logger.info({
                                                            reason: 'Canceled',
                                                            messageId: msg.id
                                                        });

                                                        let embed = reaction.message.embeds[0];

                                                        embed.setAuthor('Calcule de prime annulé', 'https://yagami.xyz/content/uploads/2018/11/discord-512-1.png', 'https://yagami.xyz')
                                                            .setColor(16711680);

                                                        msg.edit(embed);
                                                    }

                                                    msg.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
                                                }).catch((reason) => {
                                                    this._logger.error('[Downtown Cab.] Reaction validation canceled: timed out');
                                                    this._logger.error({
                                                        reason: 'Timed out',
                                                        messageId: msg.id
                                                    });

                                                    let embed = msg.embeds[0];

                                                    embed.setAuthor('Calcule de prime annulé (catch)', 'https://yagami.xyz/content/uploads/2018/11/discord-512-1.png', 'https://yagami.xyz')
                                                        .setColor(16711680);

                                                    msg.edit(embed);

                                                    msg.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
                                                });
                                        } catch (error) {
                                            console.error('Fail to react:', error);
                                        }
                                    });
                                } else {
                                    const embed: MessageEmbed = new MessageEmbed();

                                    this._logger.error(`[Downtown Cab.] Reports not founds`);
                                    this._logger.error({
                                        reason: 'Not Found',
                                        messageId: message.id
                                    });

                                    embed.setAuthor("Administration", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                                        .setDescription(`Tous les rapports de services de <@!${employee.id}> ont déjà été traité`)
                                        .setColor(16711680)
                                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();

                                    channel.send(embed);
                                }
                            }
                        });
                    } else {
                        if (args.length < 3) {
                            const content = 'Missing arguments';

                            channel.send(content);
                        } else {
                            const content = 'Invalid arguments';

                            channel.send(content);
                        }
                    }
                }

                // if (args[0] === '!c' || args[0] === '!compta') {
                //     if (args.length === 1) {
                //         // if (args[1] === 'r' || args[1] === 'recettes') {
                //         this._databaseService.getAll('bonuses/').then(data => {
                //             if (data) {
                //                 let compiledData = [];

                //                 for (let i = 0; i < Object.keys(data).length; i++) {
                //                     compiledData.push({
                //                         key: Object.keys(data)[i],
                //                         data: Object.values(data)[i]
                //                     });
                //                 }
                //                 this._logger.info(compiledData);
                //                 const timestamps: number[] = compiledData.map(v => v.data['timestamp']);
                //                 const period = {
                //                         min: this.formatTimestamp(Math.min(...timestamps)),
                //                         max: this.formatTimestamp(Math.max(...timestamps))
                //                 };
                //                 this._logger.info({
                //                     timestamps: {
                //                         min: this.formatTimestamp(Math.min(...timestamps)),
                //                         max: this.formatTimestamp(Math.max(...timestamps))
                //                     }
                //                 });

                //                 const workingTimes = compiledData.map(v => v.data['workingTime']['totalTime']);
                //                 const totalWorkingTime = this.sum(workingTimes);
                //                 this._logger.info({
                //                     totalWorkingTime: `${Math.floor(totalWorkingTime / 60)}h${totalWorkingTime % 60}m`
                //                 });

                //                 const runCount = compiledData.map(v => parseInt(v.data['runCount']));
                //                 const totalRunCount = this.sum(runCount);
                //                 this._logger.info({
                //                     totalRunCount: totalRunCount
                //                 });

                //                 const pnjAmount = compiledData.map(v => v.data['pnjAmount']);
                //                 const totalPnjAmount = this.sum(pnjAmount);
                //                 const formatedTotalPnjAmount = this.formatAmount(totalPnjAmount);
                //                 this._logger.info({
                //                     formatedTotalPnjAmount: formatedTotalPnjAmount
                //                 });

                //                 const cytizenAmount = compiledData.map(v => v.data['cytizenAmount']);
                //                 const totalCytizenAmount = this.sum(cytizenAmount);
                //                 const formatedTotalCytizenAmount = this.formatAmount(totalCytizenAmount);
                //                 this._logger.info({
                //                     formatedTotalCytizenAmount: formatedTotalCytizenAmount,
                //                 });

                //                 const totalAmount = totalPnjAmount + totalCytizenAmount;
                //                 const formatedTotalAmount = this.formatAmount(totalAmount);
                //                 this._logger.info({
                //                     formatedTotalAmount: formatedTotalAmount
                //                 });

                //                 const bonusesAmount = compiledData.map(v => v.data['prime']);
                //                 const totalBonusAmount = this.sum(bonusesAmount);
                //                 const formatedTotalBonusAmount = this.formatAmount(totalBonusAmount);


                //                 const embed: MessageEmbed = new MessageEmbed();

                //                 embed.setAuthor("Comptabilité", "https://yagami.xyz/content/uploads/2018/11/discord-512-1.png", "https://yagami.xyz")
                //                     .setDescription(`Total des dépenses`)
                //                     .setColor(8421504)
                //                     .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                //                     .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                //                     .setTimestamp();

                //                 embed.fields = [
                //                     {
                //                         'name': `Periode`,
                //                         'value': `Du ${period.min} au ${period.max}\n󠀠`,
                //                         'inline': false
                //                     },
                //                     // {
                //                     //     'name': `Temps de travail`,
                //                     //     'value': `${Math.floor(totalWorkingTime / 60)}h${totalWorkingTime % 60}m`,
                //                     //     'inline': false
                //                     // },
                //                     // {
                //                     //     'name': `Nombre de courses`,
                //                     //     'value': `${totalRunCount}\n󠀠`,
                //                     //     'inline': false
                //                     // },
                //                     {
                //                         'name': `Primes`,
                //                         'value': `${formatedTotalBonusAmount}$\n\n󠀠`,
                //                         'inline': true
                //                     }
                //                 ];

                //                 channel.send(embed);
                //             }
                //         });
                //         // }
                //     }
                // }
            }
        });
    }

    public listenManagmentMessages(): void {
        this.client.on('message', message => {
            const channel = message.channel;
            const employee = message.mentions.users.first();

            if (channel.id == env.channels.employeeManagment.id) {
                const args = message.content.split(' ');

                if (args[0] === '!i' || args[0] === '!info') {
                    if (args.length === 2) {
                        this._databaseService.getAll('reports/').then(data => {
                            if (data) {
                                let compiledData = [];

                                for (let i = 0; i < Object.keys(data).length; i++) {
                                    compiledData.push({
                                        key: Object.keys(data)[i],
                                        data: Object.values(data)[i]
                                    });
                                }

                                const result = compiledData.filter(elements => (elements.data['employeeId'] == employee.id));

                                if (result.length > 0) {
                                    const timestamps: number[] = result.map(v => v.data['timestamp']);

                                    const workingTime = result.map(v => v.data['workingTime']);
                                    const totalWorkingTime = this.compileWorkingTime(workingTime);

                                    const runCount = result.map(v => parseInt(v.data['runCount']));
                                    const totalRunCount = this.sum(runCount);

                                    const pnjAmount = this.compileAmount(result.map(v => v.data['pnjAmount']));
                                    const totalPnjAmount = this.sum(pnjAmount);
                                    const formatedTotalPnjAmount = this.formatAmount(totalPnjAmount);

                                    const cytizenAmount = this.compileAmount(result.map(v => v.data['cytizenAmount']));
                                    const totalCytizenAmount = this.sum(cytizenAmount);
                                    const formatedTotalCytizenAmount = this.formatAmount(totalCytizenAmount);

                                    const totalAmount = totalPnjAmount + totalCytizenAmount;
                                    const formatedTotalAmount = this.formatAmount(totalAmount);

                                    const embed: MessageEmbed = new MessageEmbed();

                                    let driver = this.client.users.cache.find(user => user.id === employee.id);

                                    let roles = message.mentions.members.first().roles.cache;
                                    let rolesList = "";

                                    let i = 0;
                                    roles.map(r => {
                                        i++;
                                        if (r.name != '@everyone') {
                                            if (roles.size - 1 > 1 && i < roles.size - 1) {
                                                rolesList += `${r.name},`;
                                            } else {
                                                rolesList += `${r.name}`;
                                            }
                                        }
                                    });

                                    embed.setAuthor("Informations chauffeur", driver.displayAvatarURL())
                                        .setDescription(`**Fiche employé:**\n*Nom:* <@!${driver.id}>\n*Grade:* ***${rolesList.replace(',', ' | ')}***\n*Recruté le:* ***${this.formatTimestamp(new Date(Math.min(...timestamps)))}***`)
                                        .setColor(8421504)
                                        .setFooter("Downtown Cab.", "https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setThumbnail("https://gtswiki.gt-beginners.net/decal/png/36/53/71/8152095882847715336_1.png")
                                        .setTimestamp();

                                    embed.fields = [
                                        {
                                            'name': `󠀠`,
                                            'value': `󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Periode`,
                                            'value': `Du ${this.formatTimestamp(new Date(Math.min(...timestamps)))} au ${this.formatTimestamp(new Date(Math.max(...timestamps)))}\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Temps de travail`,
                                            'value': `${totalWorkingTime.time.formated}`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Nombre de courses`,
                                            'value': `${totalRunCount}\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses PNJ`,

                                            'value': `${formatedTotalPnjAmount}$`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Courses citoyennes`,
                                            'value': `${formatedTotalCytizenAmount}$\n󠀠`,
                                            'inline': false
                                        },
                                        {
                                            'name': `Total`,
                                            'value': `${formatedTotalAmount}$\n󠀠`,
                                            'inline': true
                                        },
                                        // {
                                        //     'name': `Primes (${args[2]}%)`,
                                        //     'value': `${this.formatAmount((totalAmount * parseInt(args[2])) / 100)}$\n\n󠀠`,
                                        //     'inline': true
                                        // },
                                        {
                                            'name': `󠀠`,
                                            'value': `󠀠`,
                                            'inline': false
                                        },
                                    ];

                                    channel.send(embed);
                                }
                            }
                        });
                    } else {
                        if (args.length < 2) {
                            const content = 'Missing arguments';

                            channel.send(content);
                        } else {
                            const content = 'Invalid arguments';

                            channel.send(content);
                        }
                    }
                }
            }
        });
    }

    private sum(array: Array<number>): number {
        return array.reduce((sum, current) => sum + current, 0);
    }

    private formatAmount(amount): string {
        let formatedAmount = amount.toString().split('.')[0].split('').reverse().join('');
        let decimal = amount.toString().split('.')[1];

        let splitedAmount = formatedAmount.match(/.{1,3}/g);

        let temp = [];

        splitedAmount.forEach(el => {
            if (el.match(/(?<=^|\s)(?!000(?:\s|$))\S+(?=\s|$)/g)) {
                el = el.split('').reverse().join('');
            }
            temp.push(el);
        });

        if (decimal) {

            return `${temp.reverse().join(' ')}.${amount.toString().split('.')[1]}`;
        } else {

            return `${temp.reverse().join(' ')}`;
        }
    }

    private compileAmount(array) {
        return array.map(data => parseInt(data.replace(/\s+/g, '')));
    };

    private formatTimestamp(date) {
        let ye = new Intl.DateTimeFormat('fr', { year: 'numeric' }).format(date);
        let mo = new Intl.DateTimeFormat('fr', { month: 'short' }).format(date);
        let da = new Intl.DateTimeFormat('fr', { day: '2-digit' }).format(date);

        return `${da}-${mo}-${ye}`;
    }

    private compileWorkingTime(array): any {
        const times = array.map(data => data.replace(/[hH]/g, ' '));
        const splitedTimes = times.map(data => data.replace(/[mM]/g, ''));

        const ct = splitedTimes.map(data => data.split(' '));
        const compiledTimes = ct.map(data => {
            return data.map(value => {
                if (!value.match(/\w+$/g)) {
                    return '0';
                }

                return value;
            });
        });

        const timesInMin = compiledTimes.map(data => {
            if (data.length > 1) {
                return (parseInt(data[0]) * 60 + parseInt(data[1]))
            } else {
                return parseInt(data[0]);
            }
        });

        let minutes = this.sum(timesInMin) % 60;
        let hours = Math.floor(this.sum(timesInMin) / 60);
        let day = 0;

        if (hours > 24) {
            let d = Math.floor(hours / 24);
            hours = hours % 24
            day = d;
        }

        let formated = `${hours}h ${minutes}min`;

        if (day > 0) {
            formated = `${day}j ${hours}h ${minutes}min`;
        }

        return {
            totalTime: this.sum(timesInMin),
            time: {
                day: day,
                hours: hours,
                minutes: minutes,
                formated: formated
            }
        };
    };
}