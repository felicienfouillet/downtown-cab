import { Client } from "discord.js"
import { LoggerService } from "../logger/logger.service"
import * as env from "../../env/environment.json";

export class ReactionsListenerService {

    constructor(private client: Client, private _logger: LoggerService) {
        this._logger.info('[Downtown Cab.] Reactions listener loaded.');
    };

    public listenValidationsMessages(): void {
        this.client.on('messageReactionAdd', (reaction, user) => {
        
        });
    }
}